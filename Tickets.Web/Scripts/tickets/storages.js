﻿'use strict';

angular.module('storages', ['ngResource']).
    factory('signalR', function () {
        return new function() {
            var hub = $.connection.ticketsHub;
            this.init = function(cb) {
                hub.connection.start()
                   .done(function () {
                       cb(hub.connection.id);
                   })
                   .fail(function () { alert("Could not Connect!"); });
            };

            this.ticketUpdate = function(cb) {
                hub.client.ticketUpdated = cb;
            };
            return this;
         };
    }).
    factory('ticketStorage', function ($resource) {
        return $resource("/api/:method/:Id/", { }, {
            update: { method: 'PUT', params: { method: 'tickets', Id: '@Id'} },
            create: { method: 'POST', params: { method: 'tickets' } },
            remove: { method: 'DELETE', params: { method: 'tickets', Id: '@Id' } }
        });
    }).
    factory('dashboardStorage', function ($resource) {
        return $resource("/api/dashboard", {}, {});
    });