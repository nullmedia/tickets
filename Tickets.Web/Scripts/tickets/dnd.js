﻿'use strict';

angular.module('dnd', [])
.value('variables', {})
.directive('draggable', function (variables) {
    return {
        link: function ($scope, element) {
            element.draggable({
                revert: 'invalid',
                zIndex: 1000,
                start: function () {
                    variables.draggedItem = element;
                }
            });
        }
    };
})
.directive('droppable', function (variables) {
    return {
        link: function ($scope, element) {
            var columnId = $scope.column.Id;
            element.droppable({
                hoverClass: "column-hover",
                accept: function () {
                    if (variables.draggedItem) {
                        var ticketColumnId = angular.element(variables.draggedItem).scope().ticket.ColumnId;
                        if (ticketColumnId != columnId) {
                            return true;
                        }
                    }
                    return false;
                },
                drop: function () {
                    $scope.changeColumn(angular.element(variables.draggedItem).scope().ticket, $scope.column);
                    variables.draggedItem = null;
                }
            });
        }
    };
});