﻿'use strict';
angular.module('repositories', []);

var dashboard = angular.module('dashboard', ['dnd', 'storages']);

dashboard.config(function ($routeProvider) {
    $routeProvider.
      otherwise({ controller: 'ListCtrl', templateUrl: 'Scripts/tickets/templates/list.html' });
});

dashboard.controller('ListCtrl', function ($scope, dashboardStorage, signalR, ticketStorage) {
    var connectionId;

    signalR.init(function (id) {
        console.log(id);
        connectionId = id;
    });

    signalR.ticketUpdate(function (data) {
        console.log("signalR - ticketUpdate");

        for (var index in $scope.tickets) {
            if ($scope.tickets[index].Id == data.Id) {
                $scope.tickets[index] = data;
                generateGroupedTickets();
                $scope.$apply();
                return;
            }
        }
    });

    dashboardStorage.get(function (data) {
        $scope.columns = data.Columns;
        $scope.priorities = data.Priorities;
        $scope.users = data.Users;
        $scope.tickets = data.Tickets;
        generateGroupedTickets();
    });

    var groupedTickets = new Array();
    var generateGroupedTickets = function () {
        console.log("generateGroupedTickets");

        $scope.columns.forEach(function (column) {
            groupedTickets[column.Id] = new Array();
            $scope.priorities.forEach(function (priority) {
                groupedTickets[column.Id][priority.Id] = new Array();
            });
        });

        $scope.tickets.forEach(function (ticket) {
            groupedTickets[ticket.ColumnId][ticket.PriorityId].push(ticket);
        });
    };

    $scope.getVisiblePriorities = function (column) {
        var visiblePriorities = new Array();
        for (var index in $scope.priorities) {
            var priority = $scope.priorities[index];
            if ($scope.getTicketsByColumnAndPriority(column, priority).length) {
                visiblePriorities.push(priority);
            }
        }
        return visiblePriorities;
    };
    
    $scope.getTicketsByColumnAndPriority = function (column, priority) {
        return groupedTickets[column.Id][priority.Id];
    };

    $scope.changeColumn = function (ticket, column) {
        if (ticket.ColumnId == column.Id) {
            return;
        }
        ticket.ConnectionId = connectionId;
        ticket.ColumnId = column.Id;
        ticketStorage.update(ticket);
        generateGroupedTickets();
    };
    
    $scope.deleteTicket = function (ticket) {
        var storageObject = { Id: ticket.Id, ConnectionId: connectionId };
        ticketStorage.remove(storageObject);
        generateGroupedTickets();
    };
});