﻿using System;

namespace Tickets.Web.ViewModels.Dashboard
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int PriorityId { get; set; }
        public DateTime Deadline { get; set; }
        public int AssignedUserId { get; set; }
        public int ColumnId { get; set; }
    }
}