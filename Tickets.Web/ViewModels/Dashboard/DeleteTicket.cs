﻿namespace Tickets.Web.ViewModels.Dashboard
{
    public class DeleteTicket
    {
        public int Id {get; set; }
        public string ConnectionId { get; set; }
    }
}