﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using Tickets.Web.Hubs;
using Tickets.Web.Models;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Controllers
{
    public class TicketsController : ApiControllerWithHub<TicketsHub>
    {
        private readonly TicketRepository ticketRepository;

        public TicketsController(TicketRepository ticketRepository)
        {
            this.ticketRepository = ticketRepository;
        }

        public HttpResponseMessage Post(Ticket ticket)
        {
            ticketRepository.Add(ticket);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, ticket);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = ticket.Id }));
            return response;
        }

        public HttpResponseMessage Put(int id,PutTicket ticket)
        {
            ticketRepository.Update(ticket);

            Hub.Clients.AllExcept(ticket.ConnectionId).ticketUpdated(ticket);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Accepted, ticket);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = ticket.Id }));
            return response;
        }

        public HttpResponseMessage Delete(int id, string connectionId)
        {

            ticketRepository.Delete(id);

            Hub.Clients.AllExcept(connectionId).ticketDeleted(id);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }
    }
}
